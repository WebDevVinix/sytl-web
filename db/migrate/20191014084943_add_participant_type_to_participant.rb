class AddParticipantTypeToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :participant_type, :string
  end
end
