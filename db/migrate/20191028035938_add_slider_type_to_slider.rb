class AddSliderTypeToSlider < ActiveRecord::Migration[5.2]
  def change
    add_column :sliders, :slider_type, :string
  end
end
