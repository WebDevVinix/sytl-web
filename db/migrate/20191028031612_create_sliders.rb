class CreateSliders < ActiveRecord::Migration[5.2]
  def change
    create_table :sliders do |t|
      t.string :cover
      t.string :title
      t.text :meta_description

      t.timestamps
    end
  end
end
