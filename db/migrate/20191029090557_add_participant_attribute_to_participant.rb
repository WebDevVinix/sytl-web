class AddParticipantAttributeToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :building_name, :string
    add_column :participants, :street_name, :string
    add_column :participants, :region, :string
    add_column :participants, :city, :string
    add_column :participants, :province, :string
    add_column :participants, :phone_number, :string
    add_column :participants, :ig_account, :string
    add_column :participants, :ig_link, :string
    add_column :participants, :facebook_account, :string
    add_column :participants, :facebook_link, :string
    add_column :participants, :email, :string
    add_column :participants, :map, :text
    add_column :participants, :lat, :string
    add_column :participants, :long, :string
    add_column :participants, :image, :string
    add_column :participants, :logo, :string
  end
end
