class AddMetaDescriptionToNews < ActiveRecord::Migration[5.2]
  def change
    add_column :news, :meta_description, :text
  end
end
