class CreateCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :categories do |t|
      t.string :title
      t.string :meta_description
      t.string :slug
      t.string :parent_id

      t.timestamps
    end
  end
end
