class AddPostalCodeToParticipants < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :postal_code, :string
  end
end
