class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title
      t.text :meta_description
      t.string :slug
      t.text :content
      t.string :image
      t.datetime :publish
      t.string :status
      t.references :article_category, foreign_key: true
      t.references :slider, foreign_key: true

      t.timestamps
    end
  end
end
