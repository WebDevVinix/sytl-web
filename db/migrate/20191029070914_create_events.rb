class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.string :slug
      t.text :meta_description
      t.text :content
      t.string :start_date
      t.string :end_date
      t.string :vanue
      t.string :map
      t.string :image

      t.timestamps
    end
  end
end
