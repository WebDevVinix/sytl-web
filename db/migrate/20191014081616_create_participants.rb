class CreateParticipants < ActiveRecord::Migration[5.2]
  def change
    create_table :participants do |t|
      t.string :name
      t.string :website
      t.string :type

      t.timestamps
    end
  end
end
