class AddOverviewToParticipant < ActiveRecord::Migration[5.2]
  def change
    add_column :participants, :overview, :text
  end
end
