class RemoveTypeFromParticipant < ActiveRecord::Migration[5.2]
  def change
    remove_column :participants, :type, :string
  end
end
