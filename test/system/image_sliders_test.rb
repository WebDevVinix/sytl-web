require "application_system_test_case"

class ImageSlidersTest < ApplicationSystemTestCase
  setup do
    @image_slider = image_sliders(:one)
  end

  test "visiting the index" do
    visit image_sliders_url
    assert_selector "h1", text: "Image Sliders"
  end

  test "creating a Image slider" do
    visit image_sliders_url
    click_on "New Image Slider"

    fill_in "Caption", with: @image_slider.caption
    fill_in "Image", with: @image_slider.image
    fill_in "Slider", with: @image_slider.slider_id
    click_on "Create Image slider"

    assert_text "Image slider was successfully created"
    click_on "Back"
  end

  test "updating a Image slider" do
    visit image_sliders_url
    click_on "Edit", match: :first

    fill_in "Caption", with: @image_slider.caption
    fill_in "Image", with: @image_slider.image
    fill_in "Slider", with: @image_slider.slider_id
    click_on "Update Image slider"

    assert_text "Image slider was successfully updated"
    click_on "Back"
  end

  test "destroying a Image slider" do
    visit image_sliders_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Image slider was successfully destroyed"
  end
end
