require "application_system_test_case"

class SetupsTest < ApplicationSystemTestCase
  setup do
    @setup = setups(:one)
  end

  test "visiting the index" do
    visit setups_url
    assert_selector "h1", text: "Setups"
  end

  test "creating a Setup" do
    visit setups_url
    click_on "New Setup"

    fill_in "Site description", with: @setup.site_description
    fill_in "Site image", with: @setup.site_image
    fill_in "Site keywords", with: @setup.site_keywords
    fill_in "Site name", with: @setup.site_name
    fill_in "Site title", with: @setup.site_title
    fill_in "Site url", with: @setup.site_url
    click_on "Create Setup"

    assert_text "Setup was successfully created"
    click_on "Back"
  end

  test "updating a Setup" do
    visit setups_url
    click_on "Edit", match: :first

    fill_in "Site description", with: @setup.site_description
    fill_in "Site image", with: @setup.site_image
    fill_in "Site keywords", with: @setup.site_keywords
    fill_in "Site name", with: @setup.site_name
    fill_in "Site title", with: @setup.site_title
    fill_in "Site url", with: @setup.site_url
    click_on "Update Setup"

    assert_text "Setup was successfully updated"
    click_on "Back"
  end

  test "destroying a Setup" do
    visit setups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Setup was successfully destroyed"
  end
end
