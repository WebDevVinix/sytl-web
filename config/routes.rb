Rails.application.routes.draw do

  resources :categories
  resources :events
  resources :contacts
  resources :image_sliders
  resources :sliders
  resources :articles
  root 'pages#home'

  get 'app' => "app#index"
  get 'participants' => 'pages#participants', as: :all_participants
  get '/list-participant' => 'participants#index', as: :list_participants
  get '/news' => 'pages#news', as: :all_news
  get '/contact-us' => 'pages#contact', as: :create_contact
  get 'get-involved' => "pages#getinvolved", as: :getinvolved
  get 'say-yes-to-less-activity' => "pages#events", as: :list_event

  resources :participants do
    collection { post :import }
  end

  resources :pages
  resources :users
  resources :setups
  devise_for :users,
  path:'user',
  path_names: {
    sign_in: 'login',
    sign_out: 'logout'
  }
  # get '/participants'=> 'pages#participants', as: :participants
  get '/:id', to: 'pages#show', as: :pages_show
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
