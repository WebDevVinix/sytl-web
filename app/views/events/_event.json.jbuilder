json.extract! event, :id, :title, :slug, :meta_description, :content, :start_date, :end_date, :vanue, :map, :image, :created_at, :updated_at
json.url event_url(event, format: :json)
