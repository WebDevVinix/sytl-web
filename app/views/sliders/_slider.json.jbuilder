json.extract! slider, :id, :cover, :title, :meta_description, :created_at, :updated_at
json.url slider_url(slider, format: :json)
