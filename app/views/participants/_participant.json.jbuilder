json.extract! participant, :id, :name, :website, :type, :created_at, :updated_at
json.url participant_url(participant, format: :json)
