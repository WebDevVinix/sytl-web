json.extract! page, :id, :title, :slug, :content, :seo_title, :meta_description, :focus_keyword, :created_at, :updated_at
json.url page_url(page, format: :json)
