json.extract! article, :id, :title, :meta_description, :slug, :content, :image, :publish, :status, :article_category_id, :slider_id, :created_at, :updated_at
json.url article_url(article, format: :json)
