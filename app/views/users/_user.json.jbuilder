json.extract! user, :id, :username, :name, :role, :gender, :bio, :avatar, :created_at, :updated_at
json.url user_url(user, format: :json)
