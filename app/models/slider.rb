class Slider < ApplicationRecord
  has_many :image_sliders, inverse_of: :slider
  accepts_nested_attributes_for :image_sliders, reject_if: :all_blank, allow_destroy: true

  mount_uploader :cover, ImageUploader

  Type = ["Homepage", "Article"]

  class << self
    def article
      Slider.where(slider_type: "Article")
    end
  end

end
