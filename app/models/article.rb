class Article < ApplicationRecord
  extend FriendlyId
  friendly_id :title

  belongs_to :category
  belongs_to :slider, optional: true

  mount_uploader :image, ImageUploader

  Status = [ :publish, :draft, :trash ]

  class << self
    def news
      Article.where(category_id: 1)
    end
  end

end
