class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  extend FriendlyId
  friendly_id :username
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :validatable

  validates :username, presence: true
  validates :role, presence: true
  validates :email, presence: true

  mount_uploader :avatar, AvatarUploader

  ROLES = {root:'root',administrator:'administrator',author:'author', editor:'editor'}
  scope :root, -> {where(role: ROLES[:root])}
  scope :administrator, -> {where(role: ROLES[:administrator])}
  scope :author, -> {where(role: ROLES[:author])}
  scope :editor, -> {where(role: ROLES[:editor])}
  def is_root?
    self.role == ROLES[:root]
  end
  def is_administrator?
    self.role == ROLES[:administrator]
  end
  def is_author?
    self.role == ROLES[:author]
  end
  def is_editor?
    self.role == ROLES[:editor]
  end
end
