class ImageSlider < ApplicationRecord


  mount_uploader :image, ImageUploader
  belongs_to :slider
end
