class Page < ApplicationRecord
  extend FriendlyId
  friendly_id :title

  validates :title, presence: true
end
