class Participant < ApplicationRecord

  Type = [ 'standalone', 'hotel-restaurant', 'hotel' ]

  class << self
    def standalone
      Participant.where(participant_type: "standalone")
    end

    def hotel_restaurant
      Participant.where(participant_type: "hotel-restaurant")
    end

    def hotel
      Participant.where(participant_type: "hotel")
    end
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      new
      params =
      {participant:
        {
          name: row["title"],
          participant_type: row["type"]
        }
       }
        Participant.create(params[:participant])
      end
    end

    def self.open_spreadsheet(file)
      case File.extname(file.original_filename)
      when '.csv' then Roo::CSV.new(file.path,file_warning: :ignore)
      when '.xls' then Roo::Excel.new(file.path,file_warning: :ignore)
      when '.xlsx' then Roo::Excelx.new(file.path,file_warning: :ignore)
      else raise "Unknown file type: #{file.original_filename}"
      end
    end

end
