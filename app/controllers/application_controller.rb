class ApplicationController < ActionController::Base
  before_action :prepare_meta_tags

  private
  def prepare_meta_tags(options={})
    site_name   = 'Phoenix Communications'
    title       = 'CREATING A NEW VOICE IN MODERN PUBLISHING IN INDONESIA.'
    description = 'This has long been one of humanity’s greatest evolutionary advantages. Our ability to communicate, to convey meaning, share ideas, feelings, beliefs, principles, these are at the very centre of society and culture. It’s what makes humanity function and grow. That’s why, here at Phoenix Communications, we take communication seriously. Whether it comes in the form of sight or sound, we understand that communication is your way of engaging with society, to tell a story, share a dream and inspire. We understand that to communicate ultimately means to connect - just like we’re connecting with you right now. We hope that we can help you connect to others too.'
    keywords    = 'eco friendly, food bank'
    image       = ''
    current_url = "https://eco.nowjakarta.co.id"

    # Let's prepare a nice set of defaults
    defaults = {
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      keywords:    keywords,
      reverse: true,
      twitter: {
        site_name: site_name,
        site: '@NOW_Jakarta',
        creator: '@NOW_Jakarta',
        card: 'summary_large_image',
        description: description,
        title: title,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options
  end
end
